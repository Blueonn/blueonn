﻿using System;

namespace Задача_за_гласуване
{
    class Program
    {
        const int MINIMAL_AGE_FOR_VOTING = 18;
        const int MAXIMUM_AGE_FOR_VOTING = 100;
        static void Main(string[] args)
        {
            Console.Write("На колко години сте: ");
            int age = int.Parse(Console.ReadLine());

            if (age >= MINIMAL_AGE_FOR_VOTING &&
                age < MAXIMUM_AGE_FOR_VOTING)
            {
                Console.WriteLine("Можете да гласувате");
            }
            else
            {
                Console.WriteLine("Не можете да гласувате");
            }
        }
    }
}
